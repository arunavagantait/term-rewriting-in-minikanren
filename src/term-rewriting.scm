(load "utils.scm")

(define-extend
  (matcho t1 t2 u-in u-out)
  ((conde
    ((fresh (v res)
       (== `(var ,v) t1)
       (conde
	 ((== #f res)
	  (== `((,v ,t2) . ,u-in) u-out))
	 ((== res t2)
	  (== u-in u-out)))
       (lookupo v u-in res)))
    ((fresh (f args1 args2)
       (== `(app ,f . ,args1) t1)
       (== `(app ,f . ,args2) t2)
       (match-listo args1 args2 u-in u-out)))))
  (match-listo tl1 tl2 u-in u-out)
  ((conde
     ((== '() tl1) (== '() tl2)
      (== u-out u-in))
     ((fresh (a1 d1 a2 d2 ua ud)
	(== `(,a1 . ,d1) tl1)
	(== `(,a2 . ,d2) tl2)
	(matcho a1 a2 u-in ua)
	(match-listo d1 d2 ua u-out))))))

(define-extend
  (apply-substo t s out)
  ((conde
    ((fresh (v)
       (== `(var ,v) t)
       (conde
	 ((== out t) (lookupo v s #f))
	 ((=/= #f out) (lookupo v s out)))))
    ((fresh (f args o-args)
       (== `(app ,f . ,args) t)
       (== `(app ,f . ,o-args) out)
       (apply-subst-listo args s o-args)))))
  (apply-subst-listo tl s out)
  ((conde
    ((== '() tl) (== '() out))
    ((fresh (a1 d1 a2 d2)
       (== `(,a1 . ,d1) tl)
       (== `(,a2 . ,d2) out)
       (apply-substo a1 s a2)
       (apply-subst-listo d1 s d2))))))

;; Make it return the original term when it cannot be
;; rewritten, instead of failing
(define rewriteo
  (lambda (rule term out)
    (fresh (lt rt)
      (== rule `(,lt . ,rt))
      (fresh (mgu)
	(conde
	 ((matcho lt term '() mgu)
	  (apply-substo rt mgu out))
	 ((fresh (ctx e re)
	    (splito term ctx e)
	    (matcho lt e '() mgu)
	    (apply-substo rt mgu re)
	    (splito out ctx re))))))))

(define stepo
  (lambda (rules term out)
    (fresh (rule rest)
      (== `(,rule . ,rest) rules)
      (conde
	((rewriteo rule term out))
	((stepo rest term out))))))

(define-extend
  (not-instanceo t1 t2 s)
  ((conde
    ((fresh (v f args)
       (== `(app ,f . ,args) t1)
       (== `(var ,v) t2)))
    ((fresh (v res)
       (== `(var ,v) t1)
       (=/= #f res)
       (=/= res t2)
       (lookupo v s res)))
    ((fresh (f1 f2 args1 args2)
       (== `(app ,f1 . ,args1) t1)
       (== `(app ,f2 . ,args2) t2)
       (conde
	 ((=/= f1 f2))
	 ((== f1 f2)
	  (not-match-listo args1 args2 s)))))))
  (not-match-listo tl1 tl2 s)
  ((conde
    ((== '() tl1) (=/= '() tl2))
    ((=/= '() tl1) (== '() tl2))
    ((fresh (a1 d1 a2 d2 ua)
       (== `(,a1 . ,d1) tl1)
       (== `(,a2 . ,d2) tl2)
       (conde
	 ((not-instanceo a1 a2 s))
	 ((fresh (so)
	    (instanceo a1 a2 s so)
	    (not-match-listo d1 d2 so)))))))))

(define-extend
  (not-rewritableo lt term)
  ((conde
    ((fresh (v)
       (== `(var ,v) term)))
    ((fresh (f args)
       (== `(app ,f . ,args) term)
       (not-rewritable-listo lt args))))
   (not-instanceo lt term '()))
  (not-rewritable-listo lt tl)
  ((conde
    ((== '() tl))
    ((fresh (a d)
       (== `(,a . ,d) tl)
       (not-rewritableo lt a)
       (not-rewritable-listo lt d))))))

(define not-reducibleo
  (lambda (rules term)
    (conde
     ((== '() rules))
     ((fresh (lt rt rest)
       (== `((,lt . ,rt) . ,rest) rules)
       (not-rewritableo lt term)
       (not-reducibleo rest term))))))

(define normal-formo
  (lambda (rules term out)
    (conde
      ((== term out)
       (not-reducibleo rules term))
      ((fresh (temp)
	 (stepo rules term temp)
	 (normal-formo rules temp out))))))
