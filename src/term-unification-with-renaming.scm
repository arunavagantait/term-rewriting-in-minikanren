(load "utils.scm")

(define-extend
  (renameo term out)
  ((conde
    ((fresh (v)
       (== `(var ,v) term)
       (== `(var (,v)) out)))
    ((fresh (f args rargs)
       (== `(app ,f . ,args) term)
       (== `(app ,f . ,rargs) out)
       (rename-listo args rargs)))))
  (rename-listo tl out)
  ((conde
    ((== '() tl) (== '() out))
    ((fresh (ai di ao do)
       (== `(,ai . ,di) tl)
       (== `(,ao . ,do) out)
       (renameo ai ao)
       (rename-listo di do))))))

(define-extend
  (unify-termo t1 t2 s-in s-out)
  ((conde
    ((fresh (v a d r)
       (conde
	 ((walko t1 s-in `(var ,v))
	  (walko t2 s-in r)
	  (ext-so v r s-in s-out))
	 ((=/= 'var a)
	  (walko t2 s-in `(var ,v))
	  (walko t1 s-in `(,a . ,d))
	  (ext-so v `(,a . ,d) s-in s-out)))))
    ((fresh (f args1 args2)
       (walko t1 s-in `(app ,f . ,args1))
       (walko t2 s-in `(app ,f . ,args2))
       (unify-term-listo args1 args2 s-in s-out)))))
  (unify-term-listo tl1 tl2 s-in s-out)
  ((conde
    ((== '() tl1) (== '() tl2) (== s-in s-out))
    ((fresh (a1 d1 a2 d2 s)
       (== `(,a1 . ,d1) tl1)
       (== `(,a2 . ,d2) tl2)
       (unify-termo a1 a2 s-in s)
       (unify-term-listo d1 d2 s s-out))))))

(define-extend
  (not-occurso v t s)
  ((conde
    ((fresh (v1)
       (=/= v v1)
       (walko t s `(var ,v1))))
    ((fresh (f args)
       (== `(app ,f . ,args) t)
       (not-occurs-listo v args s)))))
  (not-occurs-listo v tl s)
  ((conde
    ((== '() tl))
    ((fresh (a d)
       (== `(,a . ,d) tl)
       (not-occurso v a s)
       (not-occurs-listo v d s))))))

(define ext-so
  (lambda (v t s-in s-out)
    (conde
      ((== `(var ,v) t)
       (== s-out s-in))
      ((fresh (v1)
	 (== `(var ,v1) t)
	 (=/= v v1)
	 (== `((,v ,t) . ,s-in) s-out)))
      ((fresh (f args)
	 (== `(app ,f . ,args) t)
	 (== `((,v ,t) . ,s-in) s-out)
	 (not-occurso v t s-in))))))
