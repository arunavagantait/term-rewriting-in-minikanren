(load "term-unification-with-renaming.scm")
(load "term-rewriting.scm")

(define cp-unifyo
  (lambda (t1 t2 s-out)
    (fresh (exp)
      (== t1 `(app . ,exp))
      (conde
	((fresh (t)
	   (== t2 `(term ,t))
	   (unify-termo t1 t '() s-out)))
	((fresh (tag t)
	   (== t2 `(,tag . ,t))
	   (=/= tag 'term)
	   (unify-termo t1 t2 '() s-out))))
      (not-alpha-substo s-out))))

(define critical-termo
  (lambda (t1 t2 out)
    (fresh (s rt2)
      (renameo t2 rt2)
      (conde
	((cp-unifyo t1 `(term ,rt2) s)
	 (walk*o t1 s out))
	((fresh (ctx e)
	   (splito t1 ctx e)
	   (cp-unifyo e `(term ,rt2) s)
	   (walk*o t1 s out)))))))

(define-extend
  (no-overlapo t1 t2)
  (conde
    ((fresh (v)
       (== t1 `(var ,v))))
    ((fresh (f args)
       (== t1 `(app ,f . ,args))
       (conde
	 ((not-unifyo t1 t2 '()))
	 ((fresh (s)
	    (unify-termo t1 t2 '() s)
	    (alpha-substo s))))
       (no-overlap-listo args t2))))
  (no-overlap-listo tl t)
  (conde
    ((== tl '()))
    ((fresh (a d)
       (== tl `(,a . ,d))
       (no-overlapo a t)
       (no-overlap-listo d t)))))

(define no-overlap-ruleso
  (lambda (rule1 rule2)
    (fresh (l1 r1 l2 r2 rl2)
      (== rule1 `(,l1 . ,r1))
      (== rule2 `(,l2 . ,r2))
      (renameo l2 rl2)
      (no-overlapo l1 rl2))))

(define-extend
  (not-unifyo t1 t2 s)
  (conde
      ((fresh (v1 f2 args)
	 (walk*o t1 s `(var ,v1))
	 (walk*o t2 s `(app ,f2 . ,args))
	 (occurs-checko v1 r2 s)))
      ((fresh (v2 f1 args)
	 (walk*o t2 s `(var ,v2))
	 (walk*o t1 s `(app ,f1 . ,args))
	 (occurs-checko v2 r1 s)))
      ((fresh (f1 f2 args1 args2)
	 (walk*o t1 s `(app ,f1 . ,args1))
	 (walk*o t2 s `(app ,f2 . ,args2))
	 (conde
	   ((=/= f1 f2))
	   ((== f1 f2)
	    (not-unify-listo args1 args2 s))))))
  (not-unify-listo tl1 tl2 s)
  (conde
    ((== tl1 '()) (=/= tl2 '()))
    ((=/= tl1 '()) (== tl2 '()))
    ((fresh (a1 d1 a2 d2)
       (== tl1 `(,a1 . ,d1))
       (== tl2 `(,a2 . ,d2))
       (conde
	 ((not-unifyo a1 a2 s))
	 ((fresh (sa)
	    (unify-termo a1 a2 s sa)
	    (not-unify-listo d1 d2 sa))))))))

(define occurs-checko
  (lambda (v t s)
    (conde
      ((walko t s `(var ,v)))
      ((fresh (f args ctx e v1)
	 (walko t s `(app ,f . ,args))
	 (splito `(app ,f . ,args) ctx `(var ,v1))
	 (occurs-checko v `(var ,v1) s))))))

(define alpha-substo
  (lambda (s)
    (conde
      ((== s '()))
      ((fresh (v val rest v1)
	 (== s `((,v (var ,v1)) . ,rest))
	 (alpha-substo rest))))))

(define not-alpha-substo
  (lambda (s)
    (fresh (var a d rest)
      (== s `((,var (,a . ,d)) . ,rest))
      (conde
	((== 'app a))
	((not-alpha-substo rest))))))

(define diff-pair-critical-pairso
  (lambda (rule1 rule2 out)
    (fresh (l1 r1 l2 r2 ct t1 t2)
      (== rule1 `(,l1 . ,r1))
      (== rule2 `(,l2 . ,r2))
      (== out `(,t1 ,t2))
      (conde
	((critical-termo l1 l2 ct))
	((critical-termo l2 l1 ct)))
      (rewriteo rule1 ct t1)
      (rewriteo rule2 ct t2))))

(define same-pair-critical-pairso
  (lambda (rule out)
    (fresh (l1 r1 ct t1 t2)
      (== rule `(,l1 . ,r1))
      (critical-termo l1 l1 ct)
      (rewriteo rule ct t1)
      (rewriteo rule ct t2)
      (fresh (f args1 args2)
	(== t1 `(app ,f . ,args1))
	(== l1 `(app ,f . ,args2)))
      (=/= t1 t2)
      (== out `(,t1 ,t2)))))

(define list-critical-pairso
  (lambda (rule rules out)
    (conde
      ((== rules '()) (== out '()))
      ((fresh (r rest cp rest-cp)
	 (== rules `(,r . ,rest))
	 (conde
	   ((diff-pair-critical-pairso rule r cp)
	    (== out `(,cp . ,rest-cp)))
	   ((no-overlap-ruleso rule r)
	    (== out rest-cp)))
	 (list-critical-pairso rule rest rest-cp))))))

(define critical-pairso
  (lambda (rules out)
    (conde
      ((== rules '()) (== out '()))
      ((fresh (rule rest cp rule-rest-cp rest-cp temp)
	 (== rules `(,rule . ,rest))
	 (conde
	   ((same-pair-critical-pairso rule cp)
	    (== temp `(,cp . ,rule-rest-cp)))
	   ((no-overlap-ruleso rule rule)
	    (== temp rule-rest-cp)))
         (list-critical-pairso rule rest rule-rest-cp)
	 (critical-pairso rest rest-cp)
	 (appendo temp rest-cp out))))))
