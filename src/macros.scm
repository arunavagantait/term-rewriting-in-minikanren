;; Some experimentation with macros to make the syntax
;; easier on the eyes:

(define-syntax def-extend
    (lambda (x)
      (define gen-ext-name
	(lambda (template-id . args)
	  (datum->syntax template-id
	    (string->symbol
	      (apply string-append
	        (map (lambda (x)
		       (if (string? x)
			   x
			   (let* ((strname (symbol->string (syntax->datum x)))
				  (strlen (string-length strname)))
			     (substring strname 0 (- strlen 1)))))
		     args))))))
      (syntax-case x ()
	[(_ name
	    (main main-args
		  main-body)
	    (list-extend list-args
			 list-body))
	 (with-syntax ([list-ext-name (gen-ext-name #'name #'name "-listo")])
	   #'(define name
	       (lambda main-args
		 (letrec ((list-ext-name (lambda list-args
					   list-body)))
		   main-body))))])))

;; Intended usage:

;; (def-extend renameo
;;   (main (term out)
;; 	(conde
;; 	  ((fresh (v-old v-new)
;; 	     (== `(var ,v-old) term)
;; 	     (== `(var ,v-new) out)
;; 	     (== `(,v-old) v-new)))
;; 	  ((fresh (f args rargs)
;; 	     (== `(app ,f . ,args) term)
;; 	     (== `(app ,f . ,rargs) out)
;; 	     (rename-listo args rargs)))))
;;   (list-extend (tl out)
;; 	       (conde
;; 		 ((== '() tl) (== '() out))
;; 		 ((fresh (ai di ao do)
;; 		    (== `(,ai . ,di) tl)
;; 		    (== `(,ao . ,do) out)
;; 		    (renameo ai ao)
;; 		    (rename-listo di do))))))

;; Problems:

;; If the programmer is having to specify the name
;; ``rename-listo'' in the clauses anyway, what do
;; I gain by creating a complicated macro that
;; generates that name?


;; Some experimentation:

(define create-name-for-list-extension
  (lambda (name)
    (let* ((strname (symbol->string name))
	   (slen (string-length strname)))
      (string->symbol
       (string-append
	(substring strname 0 (- slen 1))
	"-listo")))))

;; The following macros are not a good idea because they
;; predetermine the order of the conde clauses, removing
;; the programmer's ability to change them in order to
;; make the program more efficient.

;; (define-syntax case-term
;;   (syntax-rules ()
;;     ((_ (term)
;; 	((v) (body1 ...)
;; 	 (f args) (body2 ...)))
;;      '(conde
;; 	((fresh (v)
;; 	   (== `(var ,v) term)
;; 	   body1 ...))
;; 	((fresh (f args)
;; 	   (== `(app ,f . ,args) term)
;; 	   body2 ...))))))

;; (define-syntax case-terms
;;   (syntax-rules ()
;;     ((_ (t1 t2)
;; 	((v1 v2) (body1 ...)
;; 	 (f1 a1 f2 a2) (body2 ...)))
;;      '(conde
;; 	((fresh (v1 v2)
;; 	   (== `(var ,v1) t1)
;; 	   (== `(var ,v2) t2)
;; 	   body1 ...))
;; 	((fresh (f1 a1 f2 a2)
;; 	   (== `(app ,f1 . ,a1) t1)
;; 	   (== `(app ,f2 . ,a2) t2)
;; 	   body2 ...))))))


(define-syntax extend-for-lists
  (syntax-rules (base recur)
    ((_ args list-to-recurse
	((base
	  (body1 ...))
	 (recur
	  (body2 ...))))
     '(lambda args
	(conde
	  ((== '() list-to-recurse)
	   body1 ...)
	  ((fresh (a d)
	     (== `(,a . ,d) list-to-recurse)
	     body2 ...)))))))

(define-syntax list-extend
  (syntax-rules (base recur)
    ((_ lists
	((base) base-body)
	((recur) destruct-vars fresh-vars recur-body))
     `(conde
	,(make-base-clause lists base-body)
	(,(make-recursive-clause lists 
				 destruct-vars fresh-vars recur-body))))))

(define-syntax make-base-clause
  (syntax-rules ()
    ((_ (list ...) (body ...))
     '((== '() list) ... body ...))))

(define-syntax make-recursive-clause
  (syntax-rules ()
    ((_ (list ...) ((a d) ...) (fresh-var ...) (body ...))
     '(fresh (a ... d ... fresh-var ...)
	(== `(,a . ,d) list) ...
	body ...))))

