(load "mk-vicare.scm")
(load "mk.scm")

;; A useful macro:

(define-syntax define-extend
  (syntax-rules ()
    ((_ (name normal-arg ...) (normal-body ...)
	(lname list-arg ...) (list-body ...))
     (define name
       (lambda (normal-arg ...)
	 (letrec ((lname (lambda (list-arg ...)
			   list-body ...)))
	   normal-body ...))))))

;; Some general purpose relations, useful everywhere

;; Context + expression:

;; Generalize this definition
(define splito
  (lambda (term context expr)
    (conde
      ((fresh (v)
	 (== `(var ,v) term)
	 (== '* context)
	 (== expr term)))
      ((fresh (f args ctx)
	 (== `(app ,f . ,args) term)
	 (== `(app ,f . ,ctx) context)
	 (split-listo args ctx expr))))))

(define split-listo
  (lambda (tlist context expr)
    ;; named let or named helper function
    (letrec ((split-listo
	      (lambda (tl ctx e i-any)
		(conde
		  ((== '() tl)
      		   (== '() ctx)
		   (== '() i-any))
		  ((fresh (tla tld ctxa ctxd o-any)
		     (== `(,tla . ,tld) tl)
		     (== `(,ctxa . ,ctxd) ctx)
		     (conde
		       ((== ctxa tla)
			(== o-any i-any))
		       ((== '* ctxa)
			(=/= '* tla)    ;; This one line allows splitting contexts
			(== e tla)
			(== `(,o-any) i-any))
		       ((fresh (f args)
			  (== `(app ,f . ,args) tla)
			  (== `(,o-any) i-any)
			  (splito tla ctxa e))))
		     (split-listo tld ctxd e o-any)))))))
      (split-listo tlist context expr '(())))))

;; not-lookupo that succeeds if var is not in subst
;; then remove the conde clause from lookupo

;; split env:

;; instead of List (var, val), we have
;;            ( List var, List val )
;; var not in env -- absento var

;; webyrd/clpset-env-experiments

;; run-unique

(define lookupo
  (lambda (v subst out)
    (conde
      ((== subst '())
       ;; WEB does this mean val can't be #f?
       ;;
       ;; What about a not-in-envo?  Perhaps using a split env and
       ;; absento, as advocated by Darius Bacon.
       ;;
       ;; CLP(Set) is probably a better solution, ultimately.
       (== out #f))
      ((fresh (var val rest)
	 (== subst `((,var ,val) . ,rest))
	 (conde
	   ((== var v) (== out val))
	   ((=/= var v) (lookupo v rest out))))))))

;; Walk-ing a substitution

(define walko
  (lambda (v s out)
    (conde
      ((fresh (f args)
	 (== `(app ,f . ,args) v)
	 (== out v)))
      ((fresh (v1 res)
	 (== `(var ,v1) v)
	 (lookupo v1 s res)
	 (conde
	   ((== res #f) (== out v))
	   ((=/= res #f) (walko res s out))))))))

(define-extend
  (walk*o t s out)
  ((conde
    ((fresh (v)
       (== `(var ,v) out)
       (walko t s out)))
    ((fresh (f args wargs)
       (== `(app ,f . ,wargs) out)
       (walko t s `(app ,f . ,args))
       (walk*-listo args s wargs)))))
  (walk*-listo tl s ol)
  ((conde
    ((== tl '()) (== ol '()))
    ((fresh (ai di ao do)
       (== `(,ai . ,di) tl)
       (== `(,ao . ,do) ol)
       (walk*o ai s ao)
       (walk*-listo di s do))))))

;; Put comments / parens around (name + definition) pairs

(define appendo
  (lambda (l1 l2 out)
    (conde
     ((== l1 '()) (== l2 out))
     ((fresh (a d res)
	(== l1 `(,a . ,d))
	(== out `(,a . ,res))
	(appendo d l2 res))))))
