(load "critical-pairs.scm")

(define joino
  (lambda (rules cp out)
    (fresh (t1 t2 r1 r2)
      (== `(,cp ,t2) t1)
      (conde
	((== r1 r2)
	 (== out rules))
	((=/= r1 r2)
	 (conde
	   ((appendo rules `((,r1 . ,r2)) out))
	   ((appendo rules `((,r2 . ,r1)) out)))))
      (normal-formo rules t1 r1)
      (normal-formo rules t2 r2))))

(define join-listo
  (lambda (rules cps out)
    (conde
      ((== '() cps) (== out rules))
      ((fresh (a d newrules)
	 (== `(,a . ,d) cps)
	 (joino rules a newrules)
	 (join-listo newrules d out))))))

(define completeo
  (lambda (rules out)
    (fresh (cpl newrules)
      (conde
	((== rules newrules)
	 (== out rules))
	((=/= rules newrules)
	 (completeo newrules out)))
      (join-listo rules cpl newrules)
      (critical-pairso rules cpl))))
